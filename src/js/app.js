const elVin = document.getElementById('elVin');
const elRadioVersion = document.getElementById('elRadioVersion');
const elRadioBand = document.getElementById('elRadioBand');
const elRadioFrequency = document.getElementById('elRadioFrequency');
const elRadioHdChannel = document.getElementById('elRadioHdChannel');
const elRadioEventArtist = document.getElementById('elRadioEventArtist');
const elRadioEventTitle = document.getElementById('elRadioEventTitle');
const elRadioeEventRds = document.getElementById('elRadioeEventRds');
const elRadioIptForm = document.getElementById('elRadioIptForm');
const elRadioIptFrequency = document.getElementById('elRadioIptFrequency');
const elRadioIptBand = document.getElementById('elRadioIptBand');
const elRadioIptBtn = document.getElementById('elRadioIptBtn');
const elMediaStatus = document.getElementById('elMediaStatus');

const MSG_UNKNOWN = '<em class="warning">unknown</em>';
const MSG_NOT_APPLICABLE = '<em>n/a</em>';

// Version infos
const versionInfo = gm.info.getVersion({});
console.debug('gm.info.getVersion %o', versionInfo);
elRadioVersion.innerHTML = versionInfo.radio || MSG_UNKNOWN;

elVin.innerHTML = gm.info.getVIN() || MSG_UNKNOWN;

gm.info.getVehicleConfiguration(function(data) {
    console.debug('gm.info.getVehicleConfiguration %o', data);
});

// Buttons
function onHardButtonPress(btnList) {
    console.debug('gm.info.watchButtons %o', btnList);
}

var watchIdButtons = gm.info.watchButtons(onHardButtonPress, [
  'BTN_BACK',
  'BTN_INFO',
  'BTN_PAUSE',
  'BTN_PLAY',
  'BTN_FAV',
  'BTN_PREV',
  'BTN_NEXT',
  'SWC_PREV',
  'SWC_NEXT',
  'SB_NEXT',
  'SB_PREV',
  'SB_PLAY',
  'SB_PAUSE',
  'BTN_1',
  'BTN_2',
  'BTN_3',
  'BTN_4',
  'BTN_5',
  'BTN_6'
]);


// Radio
var watchIdRadio, watchIdRadioPlayback;
function kHzToMHz(kHz) {
    kHz = (kHz && parseInt(kHz, 10)) || 0;

    return kHz / 1000;
}

function updateRadioInfos(data) {
    const channelParts = data.station.channel.split('-');
    const frequency = kHzToMHz(channelParts[0]);

    elRadioBand.innerHTML = data.band;
    elRadioFrequency.innerHTML = frequency || MSG_UNKNOWN;
    elRadioHdChannel.innerHTML = channelParts.length === 1 ?
        MSG_NOT_APPLICABLE : channelParts[1];

    elRadioEventArtist.innerHTML = data.song.artist || MSG_UNKNOWN;
    elRadioEventTitle.innerHTML = data.song.title || MSG_UNKNOWN;
    elRadioeEventRds.innerHTML = data.song.rds || MSG_UNKNOWN;
}
// gm.info.getRadioInfo(function(data) {
//     console.debug('gm.info.getRadioInfo %o', data);
//     updateRadioInfos(data);
// });
watchIdRadio = gm.info.watchRadioInfo(function(data) {
    console.debug('gm.info.watchRadioInfo %o', data);
    updateRadioInfos(data);
});

var metaDataRadioSet = false;
function onRadioPlaybackStatus(sourcingStatus, mediaObject) {
    console.debug('onRadioPlaybackStatus %i %o',
        sourcingStatus, mediaObject);
}

function radioTuneToAm(url) {
    return gm.media.play(`AM://${url}`, '0', onRadioPlaybackStatus);
}

function radioTuneToFm(url) {
    return gm.media.play(`FM://${url}`, '0', onRadioPlaybackStatus);
}

function radioTuneToHd(url) {
    return gm.media.play(`HD://${url}`, '0', onRadioPlaybackStatus);
}

function radioTuneToStream(url) {
    return gm.media.play(url);
}

function radioTuneTo(band, url) {
    switch (band) {
        case '1':
            watchIdRadioPlayback = radioTuneToFm(url);
            break;
        case '2':
            watchIdRadioPlayback = radioTuneToAm(url);
            break;
        case '3':
            watchIdRadioPlayback = radioTuneToHd(url);
            break;
        case '4':
            watchIdRadioPlayback = radioTuneToStream(url);
            break;
    }

    return watchIdRadioPlayback;
}

function onSubmitRadioForm(event) {
    event.preventDefault();

    radioTuneTo(elRadioIptBand.value, elRadioIptFrequency.value);
}

elRadioIptForm.addEventListener('submit', onSubmitRadioForm);
